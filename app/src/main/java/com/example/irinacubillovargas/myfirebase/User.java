package com.example.irinacubillovargas.myfirebase;

/**
 * Created by Irina Cubillo Vargas on 15/5/2018.
 */

public class User {
    public String name;
    public String email;
    public String password;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String mobile) {
        this.password = mobile;
    }


    public User() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public User(String username, String email) {
        this.name = username;
        this.email = email;
    }
}
