package com.example.irinacubillovargas.myfirebase;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private static final String TAG = "Auth";

    private static final int REQUEST_IMAGE_CAPTURE  = 101;
    private static final int REQUEST_CODE = 1234;

    Button btnPhoto;
    Uri imageUri;
    String key, name, email, pass;
    ArrayList<String> matchesText;
    EditText editTextName, editTextEmail, editTextPass, startButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (! hasCamera () )
            btnPhoto.setEnabled ( false ) ;

        Button btnPhoto = (Button) findViewById(R.id.photo);

        btnPhoto.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                startRecording();
            }
        });

        mAuth = FirebaseAuth.getInstance();

        Button button = (Button) findViewById(R.id.buttonWrite);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                editTextName = (EditText) findViewById(R.id.editTextName);
                editTextEmail = (EditText) findViewById(R.id.editTextEmail);
                editTextPass = (EditText) findViewById(R.id.editTextPass);
                ImageView photo = (ImageView) findViewById(R.id.icon);

                name = editTextName.getText().toString();
                email = editTextEmail.getText().toString();
                pass = editTextPass.getText().toString();

                newUser(email,pass);

                photo.setImageURI(Uri.parse(imageUri.toString()));
            }
        });
    }

    public void verificarUser(){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("User");
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot postSnapshot: dataSnapshot.getChildren()){
                    User user = postSnapshot.getValue(User.class);
                    System.out.println("email: " + user.getEmail());
                    if(user.getEmail().equals(email)){
                        key = postSnapshot.getKey();
                        break;
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w("Nada", "loadPost:onCancelled", databaseError.toException());
                // ...
            }
        };
        myRef.addValueEventListener(postListener);
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        //updateUI(currentUser);
    }
    public void newUser(final String email1, String password){
        mAuth.createUserWithEmailAndPassword(email1, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            FirebaseDatabase database = FirebaseDatabase.getInstance();
                            DatabaseReference myRef = database.getReference("User");

                            User user=new User();
                            user.setName(name);
                            user.setEmail(email);
                            user.setPassword(pass);

                            //myRef.child(String.valueOf(email)).setValue(user);
                            myRef.push().setValue(user);
                            verificarUser();
                            upLoad();

                            editTextName.setText("");
                            editTextEmail.setText("");
                            editTextPass.setText("");

                        } else {
                            String error = ((FirebaseAuthException) task.getException()).getErrorCode();
                            if(error.equals("ERROR_EMAIL_ALREADY_IN_USE")){
                                notification();
                            }
                        }
                    }
                });
    }

    public void notification(){
        AlertDialog.Builder dialogo1 = new AlertDialog.Builder(this);
        dialogo1.setTitle("Alert");
        dialogo1.setMessage("¿ Do you want to update this user ?");
        dialogo1.setCancelable(false);
        dialogo1.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                verificarUser();
                update();
            }
        });
        dialogo1.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                dialogo1.cancel();
            }
        });
        dialogo1.show();
    }

    public void update(){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("User");

        User user=new User();
        user.setName(name);
        user.setEmail(email);
        user.setPassword(pass);

        myRef.child(String.valueOf(key)).setValue(user);
        upLoad();

        editTextName.setText("");
        editTextEmail.setText("");
        editTextPass.setText("");
    }


    private boolean hasCamera () {
        return ( getPackageManager().hasSystemFeature (PackageManager.FEATURE_CAMERA_ANY ) ) ;
    }

    public void startRecording ()
    {
        Intent takePictureIntent = new Intent ( MediaStore.ACTION_IMAGE_CAPTURE ) ;
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_IMAGE_CAPTURE){
            imageUri = data.getData();
        }
        if(requestCode == REQUEST_CODE){
            super.onActivityResult(requestCode, resultCode, data);
            matchesText = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS); //Get data of data
            startButton.setText(matchesText.get(0));
        }
    }


    public void upLoad() {
        FirebaseStorage storageRef = FirebaseStorage.getInstance();
        StorageReference storageReference = storageRef.getReferenceFromUrl("gs://myfirebase-a4d52.appspot.com");

        Toast.makeText(MainActivity.this,
                imageUri.toString(), Toast.LENGTH_SHORT).show();

        final StorageReference photoReference = storageReference.child("photos")
                .child(imageUri.getLastPathSegment());


        photoReference.putFile(imageUri)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // Get a URL to the uploaded content
                        Uri downloadUrl = taskSnapshot.getDownloadUrl();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        // ...
                        Log.d(TAG, "Fallo por :" + exception.toString());
                        Toast.makeText(MainActivity.this,
                                exception.toString(), Toast.LENGTH_SHORT).show();

                    }
                });
    }
}
